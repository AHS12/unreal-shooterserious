// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "ShooterCharacter.generated.h"

#define OUT //indicate out parameters

UCLASS()
class SHOOTERSERIOUS_API AShooterCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AShooterCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	/* Calls for forward/Backward Movement input */
	void MoveForwardBackward(float Scale);

	/* Calls for Left/Right(side to side) Movement input */
	void MoveLeftRight(float Scale);
	/*
	* Called via input to turn at a given rate
	* @param Rate This is a normalized rate, i.e 1.0 means 100% desired turn rate
	*/
	void TurnAtRate(float Rate);
	/*
	* Called via input to look up/down at a given rate
	* @param Rate This is a normalized rate, i.e 1.0 means 100% desired turn rate
	*/
	void LookUpAtRate(float Rate);

	/*
	* rotate controller based on mouse X movement
	* @param Rate input value for mouse movement
	*/
	void Turn(float Rate);
	/*
	* rotate controller based on mouse Y movement
	* @param Rate input value for mouse movement
	*/
	void LookUpDown(float Rate);

	/* called when the fire button is pressed */
	void FireWeapon();

	bool GetBeamEndLocation(const FVector& MuzzleSocketLocation, FVector& OutBeamLocation);


	/* Set bAiming to ture of false */
	void AimingButtonPressed();
	void AimingButtonReleased();

	/* set camera FOV zoom based on bAiming*/
	void CameraInterpZoom(float DeltaTime);

	/* set BaseTurnRate & BaseLookUpRate Based On bAiming*/
	void SetLookTurnRates();

	void CalculateCrossHairSpread(float DeltaTime);


	void StartCrosshairBulletFire();
	UFUNCTION()
		void FinishCrosshairBulletFire();


	void FireButtonPressed();
	void FireButtonReleased();

	void StartFireTimer();

	UFUNCTION()
		void AutoFireReset();

	/* Line trace for items under the crosshairs */
	bool TraceUnderCrosshairs(FHitResult& OUT HitResult, FVector& OUT HitLocation);

	/* Trace for item if OverlappedItemCount > 0 */
	void TraceForItems();
public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;


private:
	/* Camera boom  positioning camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadonly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class USpringArmComponent* CameraBoom;
	/* camera that follows the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadonly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* FollowCamera;

	/* Base turn rate, in deg/sec. Other scaling may affect final turn rate */
	UPROPERTY(VisibleAnywhere, BlueprintReadonly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		float BaseTurnRate;
	/* Base Look Up/Down rate, in deg/sec. Other scaling may affect final turn rate */
	UPROPERTY(VisibleAnywhere, BlueprintReadonly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		float BaseLookUpRate;
	/* turn rate while not aiming */
	UPROPERTY(EditDefaultsOnly, BlueprintReadonly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		float HipTurnRate;
	/* look up rate while not aiming */
	UPROPERTY(EditDefaultsOnly, BlueprintReadonly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		float HipLookUpRate;
	/* Turn rate while aiming */
	UPROPERTY(EditDefaultsOnly, BlueprintReadonly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		float AimingTurnRate;
	/* look up rate while aiming */
	UPROPERTY(EditDefaultsOnly, BlueprintReadonly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		float AimingLookUpRate;

	/* scale factor for mouse look sensitivity.Turn rate when not aiming */
	UPROPERTY(EditDefaultsOnly, BlueprintReadonly, Category = Camera, meta = (AllowPrivateAccess = "true"), meta = (ClampMin = "0.0", ClampMax = "1.0", UIMin = "0.0", UIMax = "1.0"))
		float MouseHipTurnRate;
	/* scale factor for mouse look sensitivity.Look up rate when not aiming */
	UPROPERTY(EditDefaultsOnly, BlueprintReadonly, Category = Camera, meta = (AllowPrivateAccess = "true"), meta = (ClampMin = "0.0", ClampMax = "1.0", UIMin = "0.0", UIMax = "1.0"))
		float MouseHipLookUpRate;
	/* scale factor for mouse look sensitivity.Turn rate when aiming */
	UPROPERTY(EditDefaultsOnly, BlueprintReadonly, Category = Camera, meta = (AllowPrivateAccess = "true"), meta = (ClampMin = "0.0", ClampMax = "1.0", UIMin = "0.0", UIMax = "1.0"))
		float MouseAimingTurnRate;
	/* scale factor for mouse look sensitivity.Look Up rate when aiming */
	UPROPERTY(EditDefaultsOnly, BlueprintReadonly, Category = Camera, meta = (AllowPrivateAccess = "true"), meta = (ClampMin = "0.0", ClampMax = "1.0", UIMin = "0.0", UIMax = "1.0"))
		float MouseAImingLookUpRate;


	/* Randomized gunshot sound*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Combat, meta = (AllowPrivateAccess = "true"))
		class USoundCue* FireSound;
	/*Flash Spawned at barrel*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Combat, meta = (AllowPrivateAccess = "true"))
		class UParticleSystem* MuzzleFlash;
	/* Animation Montage for fire weapon*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Combat, meta = (AllowPrivateAccess = "true"))
		class UAnimMontage* HipFireMontage;

	/*Particle spawned at weapon impact*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Combat, meta = (AllowPrivateAccess = "true"))
		UParticleSystem* ImpactParticle;
	/*Smoke trail for bullets*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Combat, meta = (AllowPrivateAccess = "true"))
		UParticleSystem* BeamParticle;

	/* true when aiming */
	UPROPERTY(VisibleAnywhere, BlueprintReadonly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		bool bAiming;

	/* default camera field of view */
	float CameraDefaultFOV;
	/* Filed Of View When Zoomed In */
	float CameraZoomedFOV;

	/* Current filed of view of the frame */
	float CameraCurrentFOV;
	/* Zoom In Out Interp Speed While Aiming */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Combat, meta = (AllowPrivateAccess = "true"))
		float ZoomInterpSpeed;

	/* determines the spread of the crosshairs */
	UPROPERTY(VisibleAnywhere, BlueprintReadonly, Category = Crosshairs, meta = (AllowPrivateAccess = "true"))
		float CrosshairSpreadMultiplier;
	/* velocity component for crosshiar spread */
	UPROPERTY(VisibleAnywhere, BlueprintReadonly, Category = Crosshairs, meta = (AllowPrivateAccess = "true"))
		float CrosshairVelocityFactor;
	/* in air component for crosshiar spread */
	UPROPERTY(VisibleAnywhere, BlueprintReadonly, Category = Crosshairs, meta = (AllowPrivateAccess = "true"))
		float CrosshairInAirFactor;
	/* aim component for crosshiar spread */
	UPROPERTY(VisibleAnywhere, BlueprintReadonly, Category = Crosshairs, meta = (AllowPrivateAccess = "true"))
		float CrosshairAimFactor;
	/* shooting component for crosshiar spread */
	UPROPERTY(VisibleAnywhere, BlueprintReadonly, Category = Crosshairs, meta = (AllowPrivateAccess = "true"))
		float CrosshairShootingFactor;


	float ShootTimeDuration;
	bool bFiringBullet;
	FTimerHandle CrosshairShooterTimer;


	/** Left mouse button or right console trigger pressed */
	bool bFireButtonPressed;

	/** True when we can fire. False when waiting for the timer */
	bool bShouldFire;

	/** Rate of automatic gun fire */
	float AutomaticFireRate;

	/** Sets a timer between gunshots */
	FTimerHandle AutoFireTimer;

	/* True if we should trace every frame for items */
	bool bShouldTraceForItems;

	/* Number of overlapped items(AItem) */
	int8 OverlappedItemCount;

	/* The AItem we hit last frame with line trace */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Items, meta = (AllowPrivateAccess = "true"))
		class AItem* TraceHitItemLastFrame;
public:
	/* Return CameraBoom Subobject */
	FORCEINLINE USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/* Return Follow Camera Subobject */
	FORCEINLINE UCameraComponent* GetFollowCamera() const { return FollowCamera; }

	FORCEINLINE bool GetAiming() const { return bAiming; }

	UFUNCTION(BlueprintCallable)
		float GetCrosshairSpreadMultiplier() const;

	FORCEINLINE int8 GetOverlappedItemCount() const { return OverlappedItemCount; }

	/* add/subtracts to/from overlapped item count & updates bShouldTraceForItems*/
	void IncrementOverlappedItemCount(int8 Amount);
};

