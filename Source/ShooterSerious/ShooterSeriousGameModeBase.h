// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ShooterSeriousGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SHOOTERSERIOUS_API AShooterSeriousGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
